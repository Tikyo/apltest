from kafka import KafkaConsumer
import io

log = io.open('consumer_mini.log', mode='wt', encoding='utf-8')
consumer = KafkaConsumer('apl-web', bootstrap_servers=['10.225.12.71:9092'])
for msg in consumer:
    recv = "%s:%d:%d: key=%s value=%s" % (msg.topic, msg.partition, msg.offset, msg.key, msg.value)
    log.write(recv+"\n")
    log.flush()
    print(recv)

