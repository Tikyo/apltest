#encoding=utf-8
from kafka import KafkaProducer
from kafka import KafkaConsumer
from kafka.errors import KafkaError
from datetime import datetime
from time import sleep
from random import random
import base64
import io
import os
from datetime import datetime

__topic = 'apl-web'
__KAFKA_SERVER = '10.225.12.71'
__GROUP_ID = 'Python-client'
__log = open(os.path.abspath(('./%s.log'%datetime.now().isoformat()).replace(':','-')), mode='wt')


def logger():
    global __log
    return __log


def on_send_success(args):
    """
    """
    logger().writelines('sent success. offset: %s'%args.offset)


def on_send_error(err):
    """
    """
    logger().writelines('sent err. offset: %s'%err.offset)


producer = KafkaProducer(bootstrap_servers=['10.225.12.71'], retries=3)
# block until all async messages are sent

def heartbeat():
    '''
    模拟心跳包
    '''
    status = ['idle', 'running', 'pause', 'resume']
    rst = int(random() * 4)
    hb = """{
        "part_number":"96xl",
        "serial_number":"sz8393",
        "inputs":{
            "instrument_status":"%s",
            "disk":{
                "total":"",
                "used":""
            },
            "temperature":{
                "motor_temperature":"",
                "motherboard_temperature":"",
                "liquor_temperature":""
            }
            }
        }"""%status[rst]
    return hb


def send(msg):
    """
    发送到kafka
    return send-check kafka result
    """
    global producer, __topic
    try:
        logger().writelines([msg, '\n'])
        r = producer.send(__topic, msg.encode('ascii')).add_callback(on_send_success).add_errback(on_send_error)
        return r.get(timeout=20)
    except KafkaError as e:
        logger().writelines(e.message + '\n')


def send_wait(msg, if_my_response, timeout=20):
    """
    发送命令，等待反馈.
    msg: send msg to kafka
    if_my_response 是判断响应是否预期的函数
    timeout: wait response timeout seconds
    """
    global __topic, __KAFKA_SERVER, __GROUP_ID
    sent_result = send(msg)
    consumer1 = KafkaConsumer(__topic,
                          group_id=__GROUP_ID,
                          bootstrap_servers=__KAFKA_SERVER, consumer_timeout_ms=timeout*1000,
                          auto_offset_reset='latest')
    


__fmt_task = u'''{
                    "message_type":"task_issue",
                    "message_group":"PYTHON-LIMS",
                    "message_content":{
                        "task_id":"%s",
                        "task_class":"sampling",
                        "module_id":"9",
                        "device_type":"MGISP-96XL-ex",
                        "device_id":"python-simulated-host"
                    },
                    "message_id":"%s"
                }'''


__fmt_script = u'''{
                    "message_type":"command",
                    "message_group":"PYTHON-LIMS",
                    "message_content":{
                        "task_id":"%s",
                        "task_class":"sampling",
                        "module_id":"9",
                        "device_type":"MGISP-96XL-ex",
                        "device_id":"python-simulated-host",
                        "command_id":null,
                        "command":"run",
                        "parameters":{
                            "script_id":"",
                            "script_code":"BASE64",
                            "script_name":"%s",
                            "script_content":"%s",
                            "script_version":"APL-WEB",
                            "script_owner":"PYTHON-W",
                            "script_desc":"TEST-SCRIPT",
                            "final_script":"%s",
                            "arguments":\{\}}},
                            "message_id":"%s"}
'''

__fmt_clear = '''{
                    "message_type":"task_issue",
                    "message_group":"PYTHON-LIMS",
                    "message_content":{
                        "task_id":"1",
                        "task_class":"sampling",
                        "module_id":"9",
                        "device_type":"MGISP-96XL",
                        "device_id":"x960",
                        "command_id":"",
                        "command":"clear",
                        "parameters":{
                            "clear_type":2
                        }
                    },
                    "message_id":"%s"
                }'''

global __TASK_ID

def task(tid):
    """
    创建一个task
    """
    global __fmt_task, producer, __TASK_ID, f, __topic
    msg = __fmt_task%(tid, str(datetime.now()))
    send(msg)
    __TASK_ID = tid


import os.path

def b64encode(path):
    '''
    将脚本编码成base64
    return (fname, base64)
    '''
    abs = os.path.abspath(path)
    logger().writelines(abs + '\n')
    buff = ''
    with io.open(abs, mode='rt', encoding='utf-8') as f:
        for row in f.readlines():
            buff = buff + row        
        result = base64.encodestring(buff.encode('utf-8'))
        return (os.path.basename(path), result)



def start(path, if_final=False):
    """
    开始一个96xl task
    """
    info = b64encode(path)
    global __fmt_script, __TASK_ID, f
    if if_final:
        msg = __fmt_script%(__TASK_ID, info[0], info[1], 'yes', str(datetime.now()))
    else:
        msg = __fmt_script%(__TASK_ID, info[0], info[1], 'no', str(datetime.now()))
    send(msg)



def pause():
    """
    暂停96xl task
    """
    pass



def resume():
    """
    恢复96xl task
    """
    pass


def stop():
    """
    stop
    """
    pass


def clear():
    """
    clear task
    """
    global __fmt_clear
    msg = __fmt_clear%str(datetime.now())
    send(msg)



if __name__ == "__main__":
    try:
        for i in range(1, 100):
            task(i)
            sleep(i * 0.2)
            start('spx96_nifty_step1_cn.py')
    except Exception as e:
        logger().writelines(u'error: %s\n'%e.message)
    finally:
        logger().flush()
        logger().close()
    


# Block for 'synchronous' sends
# try:
#     for i in range(1, 10000000):
#         # Asynchronous by default
#         intval = 0.1
#         if i > 10:
#             intval = 7 * random()
        
#         future = producer.send('apl-web', heartbeat().encode('ascii'))
#         #print(future)
#         record_metadata = future.get(timeout=10)
#         #print (record_metadata.topic)
#         #print (record_metadata.partition)
#         #print (record_metadata.offset)      
#         f.writelines(['message (%d) sent, count %d, next intval %fs'%(record_metadata.offset, i, intval)])
#         sleep(intval)
# except KafkaError:
#     # Decide what to do if produce request failed...
#     log.exception()
#     pass

# print 'completion!'
# f.flush()
# f.close()
# Successful result returns assigned partition and offset


# # produce keyed messages to enable hashed partitioning
# producer.send('my-topic', key=b'foo', value=b'bar')

# # encode objects via msgpack
# producer = KafkaProducer(value_serializer=msgpack.dumps)
# producer.send('msgpack-topic', {'key': 'value'})

# # produce json messages
# producer = KafkaProducer(value_serializer=lambda m: json.dumps(m).encode('ascii'))
# producer.send('json-topic', {'key': 'value'})

# # produce asynchronously
# for _ in range(100):
#     producer.send('my-topic', b'msg')

# def on_send_success(record_metadata):
#     print(record_metadata.topic)
#     print(record_metadata.partition)
#     print(record_metadata.offset)

# def on_send_error(excp):
#     log.error('I am an errback', exc_info=excp)
#     # handle exception

# # produce asynchronously with callbacks
# producer.send('my-topic', b'raw_bytes').add_callback(on_send_success).add_errback(on_send_error)

# # block until all async messages are sent
# producer.flush()

# # configure multiple retries
# producer = KafkaProducer(retries=5)
