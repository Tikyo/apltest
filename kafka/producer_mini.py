import json
from kafka import KafkaProducer

producer = KafkaProducer(bootstrap_servers='10.225.12.71:9092')

msg_dict = {
    "msg": "hello world!"
}
msg = json.dumps(msg_dict)
print msg
i = 1
for i in range(1, 100):
    producer.send('apl-web', msg, partition=0)
    i = i + 1
producer.close()
