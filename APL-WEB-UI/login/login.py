# coding:utf-8
import os  # 引入os模块
import csv  # 引入csv包
from time import sleep  # 引入sleep方法
from selenium import webdriver  # 从selenium中引入webdriver

list_username = []
list_password = []
data = csv.reader(file('D:\\apltest\\APL-WEB-UI\\data\\userConfig.csv', 'rU'))  # 获取每列数据

# print(data)
for user in data:
    # print user[0]  # 获取第一列中从上往下的数据
    list_username.append(user[0])  # 将每次读取的字符串加入到数组中
    # print user[1]  # 获取第二列中从上往下的数据
    list_password.append(user[1])  # 将每次读取的字符串加入到数组中


def login_mantis_by_csv():
    for i in range(len(list_username)):
        driver = webdriver.Chrome()  # 选择打开的浏览器
        # driver.maximize_window()  # 浏览器窗口最大化
        driver.implicitly_wait(3)  # 隐式等待
        # driver.get("http://192.168.1.201/mantisbt-1.2.19/login_page.php")  # 获取URL，打开页面
        driver.get("http://localhost:5000")
        sleep(1)  # 直接等待
        username_input = driver.find_element_by_id("UserName")
        username_input.send_keys(str(list_username[i]))  # 输入账号
        sleep(1)
        password_input = driver.find_element_by_id("Password")
        password_input.send_keys(str(list_password[i]))  # 输入密码
        sleep(1)
        # login_button = driver.find_element_by_xpath("html/body/div[3]/form/table/tbody/tr[6]/td/input")  # 获取登录按钮
        login_button = driver.find_element_by_id("btnLogin")
        login_button.click()  # 点击登录按钮
        sleep(3)
        driver.quit()  # 退出浏览器


if __name__ == '__main__':
    login_mantis_by_csv()