# coding:utf-8
import os  # 引入os模块
import csv  # 引入csv包
from time import sleep  # 引入sleep方法
from selenium import webdriver  # 从selenium中引入webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

list_deviceName = []
list_deviceCode = []
list_deviceSN = []
list_deviceIcon = []
list_deviceRemarks = []

data = csv.reader(file('D:\\APL_test\\APL-WEB\\data\\deviceMgr.csv', 'rU'))  # 获取每列数据
# data = csv.reader('D:\\APL_test\\APL-WEB\\deviceMgr.csv', 'rU')

# print(data)
for device in data:
    list_deviceName.append(device[0])  # 将每次读取的字符串加入到数组中
    list_deviceCode.append(device[1])  # 将每次读取的字符串加入到数组中
    list_deviceSN.append(device[2])
    list_deviceIcon.append(device[3])
    list_deviceRemarks.append(device[4])


def add_device_by_csv():
    driver = webdriver.Chrome()
    driver.get("https://localhost:5001/DeviceInfo")
    # login
    username_input = driver.find_element_by_id("UserName")
    username_input.send_keys("admin")  # 输入账号
    # sleep(1)
    password_input = driver.find_element_by_id("Password")
    password_input.send_keys(123456)  # 输入密码
    # sleep(1)
    login_button = driver.find_element_by_id("btnLogin")
    login_button.click()  # 点击登录按钮

    # goto deviceMgr
    driver.find_element_by_link_text("设备管理").click()
    sleep(1)

    for i in range(len(list_deviceName)-1):
        add_button = driver.find_element_by_id("btnAdd")
        add_button.click()
        sleep(1)
        # deviceName_input = driver.find_element_by_css_selector("")
        deviceName_input = driver.find_element_by_id("DeviceName")
        deviceName_input.send_keys(str(list_deviceName[i+1]))

        # deviceName_input.send_keys(Keys.TAB)

        sleep(1)
        deviceCode_input = driver.find_element_by_id("DeviceCode")
        deviceCode_input.send_keys(str(list_deviceCode[i+1]))

        sleep(1)
        deviceSN_input = driver.find_element_by_id("SerialNumber")
        deviceSN_input.send_keys(str(list_deviceSN[i+1]))

        sleep(1)
        deviceIcon_input = driver.find_element_by_id("Icon")
        deviceIcon_input.send_keys(str(list_deviceIcon[i+1]))

        sleep(1)
        deviceRemark_input = driver.find_element_by_id("Remarks")
        deviceRemark_input.send_keys(str(list_deviceRemarks[i+1]))

        sleep(1)
        save_button = driver.find_element_by_id("btnSave").click()

        sleep(3)


if __name__ == '__main__':
    add_device_by_csv()