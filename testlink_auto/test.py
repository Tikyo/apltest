# coding:utf-8
import urllib2
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time, random
import re, os
import yaml
url = "file:///D:/APL_test/testlink_auto/testplan.html"
url2 = "file:///D:/APL_test/testlink_auto/testreport2.html"
a = urllib2.urlopen(url).read()
# print(a)
try:
    f = open('testlinkauto.yaml')
    datadict = yaml.load(f)
    testconfig = datadict['testlink']
except Exception, e:
    print "The error is :", e

# start = time.clock()
# chrome_driver_path = r'E:\SPX_master\venv\Scripts\chromedriver.exe'  # 这里使用chrome，所以必需下载chromedriver驱动，否则无法执行
# browser = webdriver.Chrome(chrome_driver_path)
# print testconfig.get('address')
# browser.get(testconfig.get('address'))


def getTestResult(url):
    date = urllib2.urlopen(url)
    data = date.read()
    global caselist
    varpatten = '测试用例 (' + testconfig['testprojectid'] + '-\d+)'
    print varpatten
    caselist = re.findall(varpatten, data)
    # caselist=re.findall('测试用例 (GX-\d+)',data)
    resultlist = re.findall('执行备注.*?">(.*)</td', data)
    return dict([(x, y) for x in caselist for y in resultlist])

b = getTestResult(url2)
print(b)