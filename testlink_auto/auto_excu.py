# coding:utf-8
'''
Created on 2019-3-11
@author: yangzhiqiong@genomics.cn
'''
from selenium import webdriver
from selenium.webdriver.support.ui import Select
import time, random
import re, os
import urllib2
import yaml

print os.path.dirname(__file__)  # 获取当前文件绝对路径
try:
    f = open('testlinkauto.yaml')
    datadict = yaml.load(f)
    testconfig = datadict['testlink']
except Exception, e:
    print "The error is :", e

start = time.clock()
chrome_driver_path = r'E:\SPX_master\venv\Scripts\chromedriver.exe'  # 这里使用chrome，所以必需下载chromedriver驱动，否则无法执行
browser = webdriver.Chrome(chrome_driver_path)
print testconfig.get('address')
browser.get(testconfig.get('address'))

# 登陆
browser.find_element_by_id('login').clear()
browser.find_element_by_id('login').send_keys(testconfig.get('user'))
browser.find_element_by_name('tl_password').send_keys(testconfig.get('password'))
browser.find_element_by_name('login_submit').click()
time.sleep(2)


# 执行用例程序
def testCaseWrite(testlist):
    for case in testlist:
        print "%s The case is %s and result is pass" % (time.strftime('%Y-%m-%d %H:%M:%S'), case)
        browser.switch_to_default_content()  # 切回默认的frame
        browser.switch_to_frame('mainframe')  # 切到主frame
        # 切到左侧例表frame
        browser.switch_to_frame('treeframe')
        # 选择要执行的用例
        browser.find_element_by_name('filter_tc_id').clear()
        browser.find_element_by_name('filter_tc_id').send_keys(case)
        browser.find_element_by_id('doUpdateTree').click()
        browser.find_element_by_id('expand_tree').click()
        time.sleep(2)
        # browser.find_elements_by_css_selector("//a[@href='javascrpit:ST(29,30)']").click()
        # browser.find_element_by_xpath('//*[@id="ext-gen21"]/li/div/a').click()
        # browser.find_element_by_xpath('//*[@id="ext-gen21"]/li/div/a').click()
        browser.find_element_by_xpath("//a[contains(@href,'javascript:ST')]").click()  # 点击执行
        time.sleep(5)

        # 切换至右侧的用例frame
        browser.switch_to_default_content()
        browser.switch_to_frame('mainframe')
        browser.switch_to_frame('workframe')

        # 写执行记录并确定提交
        # browser.find_element_by_class_name('step_note_textarea').send_keys(u'成功')
        # browser.find_element_by_id('notes').send_keys(u'成功')
        # browser.find_element_by_xpath("//textarea[starts-with(@name,'notes')]").send_keys(result.decode('utf-8'))
        # browser.find_element_by_id("status_88535_p").click()
        # time.sleep(2)

        # Select(browser.find_element_by_name('statusSingle')).select_by_value("p") #选择项目
        # Select(browser.find_element_by_xpath("//select[contains(@name,'statusSingle')]")).select_by_value("p")
        browser.find_element_by_xpath("//input[contains(@value,'p')]").click()
        # //*[@id="step_status_31"]
        time.sleep(1)
        browser.find_element_by_xpath("//input[contains(@name,'save_results')]").click()
        time.sleep(random.uniform(2, 4))

 # 获取测试计划用例列表并去重排序
def getTestPlan(url):
    date = urllib2.urlopen(url).read()
    varpatten = testconfig['testprojectid'] + '-\d{1,5}'
    testlist = re.findall(varpatten, date)
    aa = list(set(testlist))
    aa.sort(key=lambda x: int(re.match('\D+-(\d+)', x).group(1)))  # 根据ID来排序
    return aa


# 获取原来测试报告中用例及执行结果对应字典关系
def getTestResult(url):
    date = urllib2.urlopen(url)
    data = date.read()
    global caselist
    varpatten = '测试用例 (' + testconfig['testprojectid'] + '-\d+)'
    print varpatten
    caselist = re.findall(varpatten, data)
    # caselist=re.findall('测试用例 (GX-\d+)',data)
    resultlist = re.findall('执行备注.*?">(.*)</td', data)
    return dict([(x, y) for x in caselist for y in resultlist])


if __name__ == '__main__':
    # 定位工具栏选项
    browser.switch_to_frame('titlebar')  # 切换到iframe为titlebar上
    print browser.current_url
    # browser.find_element_by_name('testproject').click()
    # 遍历下拉框并选择需要的项目
    select = browser.find_element_by_name("testproject")
    allOptions = select.find_elements_by_tag_name("option")

    for option in allOptions:
        print "Value is: " + option.get_attribute("value")
        print "Text is:" + option.text
        if testconfig['testprojectid'] in option.text:
            option.click()
            break

    time.sleep(5)
    # browser.find_element_by_xpath("//img[@title='执行']").click()  # 执行用例
    # browser.find_elements_by_link_text('lib/general/frmWorkArea.php?feature=executeTest').click()
    browser.find_element_by_link_text("执行").click()
    browser.switch_to_default_content()  # 切回默认的frame
    browser.switch_to_frame('mainframe')  # 切到主frame
    print browser.current_url

    # 执行程序
    TestPlanCase = getTestPlan(testconfig['testplanurl'])
    TestResultCase = getTestResult(testconfig['testreporturl'])
    print "testPlanCase:%s" % TestPlanCase
    print "testResultCase:%s"%TestResultCase

    # 未有结果的列表并排序
    print "计划中要执行的用例列表：", list(set(TestPlanCase))
    print "原测试报告中已执行的用例列表：", list(set(caselist))
    unuse = set(TestPlanCase) ^ set(caselist)
    unuselist = list(unuse)
    unuselist.sort(key=lambda x: int(re.match('\D+-(\d+)', x).group(1)))

    print "此次未执行的case列表为：", unuselist
    print "总共%s个!" % len(unuselist)
    print "大约要花费%d分钟" % (len(unuselist) * 14 / 60)

    # 执行
    print TestResultCase
    # testCaseWrite(TestResultCase)
    testCaseWrite(unuselist)
    end = time.clock()
    print "执行结束,总共花费%6.2f 秒时间" % (end - start)

'''       
    #定位测试计划并选用
    browser.switch_to_default_content()
    browser.switch_to_frame('mainframe')
    print browser.current_url
    print testconfig['testplan']
    browser.find_element_by_xpath("//div[contains(@class,'chosen-c')]/div/ul").click
    #browser.find_element_by_xpath("//div[contains(@class,'chosen-c')]/div/ul/li[3]").click
   #browser.find_element_by_xpath("//div[contains(@class,'chosen-c')]").click()
    #WebDriverWait(browser,10).until(lambda the_driver: the_driver.find_element_by_tag_name('li').is_displayed())
    #menu=browser.find_element_by_tag_name('li').find_element_by_link_text('GXVCP_IOS_D10V100_rc4')
    #webdriver.ActionChains(browser).move_to_element(menu).perform()
    #drop_down=browser.find_element_by_xpath("//div[contains(@class,'chosen-c')]/div/ul")
    print drop_down
    time.sleep(1)
    #browser.find_element_by_link_text('GXVCP_IOS_D10V100_rc4')

    allOptions1 = drop_down.find_elements_by_tag_name("li")
    print allOptions1
    drop_down.find_element_by_link_text('GXVCP_IOS_D10V100_rc4')
    browser.find_element_by_name('testplan')
    Select(browser.find_element_by_name('testplan')).select_by_visible_text(testconfig['testplan'])

'''
