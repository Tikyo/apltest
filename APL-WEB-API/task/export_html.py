# -*- coding: UTF-8 -*-

"""
Created on 2019-03-19
Project: learning
@Author:
"""

import os, time, unittest,sys
import HTMLTestRunner
if sys.getdefaultencoding() != 'utf-8':
    reload(sys)
    sys.setdefaultencoding('utf-8')

report_path = os.getcwd()  # 设置保存报告的路径，这儿设置的是与执行文件在同一个目录下
now = time.strftime("%Y-%m-%d", time.localtime(time.time()))  # 获取当前时间 %H:%M
title = str("Title")  # 标题
print title
report_abspath = os.path.join(report_path, title + now + ".html")  # 设置报告存放和命名


# 导入用例
def all_case():
    case_path = os.getcwd() # 用例路径，这儿的用例和执行文件在同一目录下
    discover = unittest.defaultTestLoader.discover(case_path, pattern="Test*.py")
    # 添加用例，在case_path的路径下，所有以Test开头的文件都当做用例文件执行
    print(discover)
    return discover


if __name__ == "__main__":
    fp = open(report_abspath, "wb") # 保存报告文件
    runner = HTMLTestRunner.HTMLTestRunner(stream=fp, title=title + '：')
    runner.run(all_case())  # 执行用例
    fp.close()
