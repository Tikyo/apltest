#encoding=utf-8
from kafka import KafkaProducer
from kafka import KafkaConsumer
from kafka.errors import KafkaError
from datetime import datetime
from time import sleep
from random import random
import base64
import io
import os
import os.path

__topic = 'apl-webs'
__KAFKA_SERVER = '10.225.12.71:9092'
__TASK_ID = (datetime.now().isoformat()).replace(':', '-')
# __GROUP_ID = 'Python-client'
__log = open(os.path.abspath(('./logs/%s.log' % datetime.now().isoformat()).replace(':', '-')), mode='wt')
# __log = open(os.path.abspath(('./%s.log'%datetime.now().isoformat()).replace(':', '-')), mode='wt')

producer = KafkaProducer(bootstrap_servers=['10.225.12.71:9092'], retries=3)


def logger():
    global __log
    return __log


def on_send_success(args):
    logger().writelines('send success. offset: %s' % args.offset)


def on_send_error(err):
    logger().writelines('send err. offset: %s' % err.offset)


__fmt_task = u'''{
    "message_id": "UUID",
    "message_type": "task_issue",
    "message_group": "task-lims",
    "message_content": {
        "task_id": "%s",
        "task_class": "",
        "module_id": "7",
        "device_type": "%s",
        "device_id": "%s"
    }
}
'''

__fmt_script = u'''{
    "message_id": "UUID",
    "message_type": "command",
    "message_group": "task_lims",
    "message_content": {
        "task_id": "%s",
        "task_class": "",
        "module_id": "7",
        "device_type": "%s",
        "device_id": "%s",
        "command_id": "",
        "command": "run",
        "parameters": {
            "script_code": "",
            "script_name": "%s",
            "script_content": "%s",
            "final_script": "%s",
            "arguments": {}
        }
    }
}'''


def script_command(rst):
    # 脚本命名包括暂停、恢复、停止
    # rst = 0 暂停 1 恢复 2 停止
    command = ['pause', 'resume', 'stop']
    # rst = int(random()*3)
    comm = '''{
        "message_id": "UUID",
        "message_type": "command",
        "message_group": "task_lims",
        "message_content": {
            "task_id": "%s",
            "task_class": "",
            "module_id": "7",
            "device_type": "%s",
            "device_id": "%s",
            "command_id": "",
            "command": "%s",
            "parameters": {}
        }
    }
    ''' % (__TASK_ID, __DEVICE_TYPE, __DEVICE_ID, command[rst])
    return comm


def send(msg):
    global producer, __topic
    try:
        logger().writelines([msg, '\n'])
        r = producer.send(__topic, msg.encode('ascii')).add_callback(on_send_success).add_errback(on_send_error)
        return r.get(timeout=20)
    except KafkaError as e:
        logger().writelines(e.message + '\n')


def create_task(tid, device_type, device_id):
    global __fmt_task, __TASK_ID, __topic
    msg = __fmt_task % (tid, device_type, device_id)
    send(msg)
    __TASK_ID = tid


def b64encode(path):
    abs = os.path.abspath(path)
    logger().writelines(abs + '\n')
    buff = ''
    with io.open(abs, mode='rt', encoding='utf-8') as f:
        for row in f.readlines():
            buff = buff + row
        result = base64.encodestring(buff.encode('utf-8'))
        return (os.path.basename(path), result)


def start_script(device_type, device_id, path, if_final=False):
    info = b64encode(path)
    global __fmt_script, __TASK_ID, __DEVICE_TYPE, __DEVICE_ID
    if if_final:
        msg = __fmt_script % (__TASK_ID, device_type, device_id, info[0], info[1], 'yes')
    else:
        msg = __fmt_script % (__TASK_ID, device_type, device_id, info[0], info[1], 'no')
    send(msg)
    __DEVICE_TYPE = device_type
    __DEVICE_ID = device_id


if __name__ == "__main__":
    try:
        for i in range(1, 2):
            # create_task(201903280777704)
            # sleep(i * 0.2)
            start_script('Spx960', 'device000000SPX', 'spx96_nifty_step1_cn.py')
            # # 暂停
            # send(script_command(0))
            # # 恢复
            # send(script_command(1))
            # # 停止
            # send(script_command(2))
    except Exception as e:
        logger().writelines(u'error: %s\n' % e.message)
    finally:
        logger().flush()
        logger().close()
