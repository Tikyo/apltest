# -*- coding: UTF-8

import os
from kafka_device_command import send
from kafka_device_command import device_command
from kafka_device_operation import device_operation
from kafka_script_command import start_script_nifty1
from kafka_script_command import create_task


# os.system("python ./api_device_register.py>>log.txt")
# os.system("python ./api_device_heartbeat.py>>log.txt")
# 设备上线
# os.system("python ./kafka_heartbeat.py>>heartbeat.log")
# 解锁
send(device_command(0))
# 物料装载
# send(device_operation(0))
# 上锁
send(device_command(1))
# 扫码
send(device_command(2))
# 库存更新

# 取出物料
send(device_operation(2))
# 工位1运行脚本
create_task(1)
start_script_nifty1("spx96_nifty_step1_cn.py")

# 工位2运行脚本

# 脚本完成
