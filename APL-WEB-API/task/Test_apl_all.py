# --*-- coding:utf-8 --*--

import requests
import unittest
import time


class TestXjkLinks(unittest.TestCase):

    def test_learn(self):
        # u"""首页学习接口"""
        url = 'http://www.baidu.com/'
        result = requests.get(url)
        time.sleep(1)
        # 响应状态码断言
        real_code = result.status_code
        self.assertEqual(200, real_code)
        # 未登陆时文案断言
        # real_text = result.text
        # expect_text = '使用用户名密码登录'
        # self.assertIn(expect_text, real_text)

    def test_explore(self):
        # u"""首页发现接口"""
        url = 'http://www.baidu.com/'
        result = requests.get(url)
        time.sleep(1)
        # 响应状态码断言
        real_code = result.status_code
        self.assertEqual(200, real_code)
        # # 文案断言
        # real_text = result.text
        # expect_text = '有趣的作品'
        # self.assertIn(expect_text, real_text)
    #
    # def test_login(self):
    #     u"""首页登陆接口"""
    #     url = 'http://www.xiaojike.cn/sign_in/'
    #     result = requests.get(url)
    #     time.sleep(1)
    #     # 响应状态码断言
    #     real_code = result.status_code
    #     self.assertEqual(200, real_code)
    #     # 文案断言
    #     real_text = result.text
    #     expect_text = '注册不到1秒钟'
    #     self.assertIn(expect_text, real_text)
    #
    # def test_err(self):
    #     u"""判断错误的例子"""
    #     self.assertNotEqual(3, 3)


if __name__ == '__main__':
    unittest.main()
    # print('小极客首页接口测试成功')