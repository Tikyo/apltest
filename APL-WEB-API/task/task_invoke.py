# -*- coding: UTF-8

import requests
import json
import urllib.parse
import base64

server = {
    "host": "http://zlims-isw-01.genomics.cn",
    "port": "8000",
    "username": "zlims_be",
    "password": "zlims_be"
}

headers = {'content-type': 'application/json'}

apis = {
    "version": "/zlims/utils/version",
    "rcmd_invoke": "/zlims/rcmd/invoke"
}

# script_path = "3.Nifty_2板ER+LA+纯化+PCRmix_20180911.py"
script_path = "E:/输出文档/项目输出/SPX/远程/4-2.Nifty_2板PCR反应_20190103.py"

with open(script_path, encoding="utf8") as script:
    script_data = script.read()
    script_content = base64.b64encode(urllib.parse.quote(script_data).encode()).decode('utf-8')


if script_content:
    payload = '''
    {
        "caller_id": "yzq",
        "part_number": "MGISP-960",
        "serial_number": "SZ13410_10-GENOMICS",
        "command": "start",
        "cmd_payload": {
            "script_id": "<string>",
            "script_code": "<string>",
            "script_name": "<string>",
            "script_content": "4-2.Nifty_20190103.py",
            "script_version": "<string>",
            "script_owner": "<string>",
            "script_desc": "<string>",
            "final_script": "%s",
            "arguments": {}
        },
        "invoke_date": "2017-03-08T04:52:02Z"

    }
    ''' % script_content

    url = server["host"] + ":" + server["port"] + apis["rcmd_invoke"]
    r = requests.post(url, data=payload, auth=(server["username"], server["password"]), headers=headers)
    print(r.json())
