# -*- coding: UTF-8

import os
from kafka_device_command import send
from kafka_device_command import device_command
from kafka_device_operation import device_operation
from kafka_script_command import start_script
from kafka_script_command import create_task
from kafka_script_command import script_command
import time
from datetime import datetime


# # 解锁
# send(device_command(0))
# 上锁
# send(device_command(1))
# 扫码
# send(device_command(2))
#
# 任务下达
create_task('PlateLoc', 'NIFTY-sz1-yzq', (datetime.now().isoformat()).replace(':', '-'))
#
# 运行脚本
start_script('PlateLoc', 'NIFTY-sz1-yzq', 'spx96_nifty_step1_cn.py')
# 暂停
# send(script_command(0))
# # 恢复
# send(script_command(1))
# # 停止
send(script_command(2))


# 工位2运行脚本

# 脚本完成
